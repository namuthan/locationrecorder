# ***Overall Picture*** #

Location Recorder is an under-developing android app.
It's main functionality is to manage tasks based on locations.
For example, If I want to do task A when I get to place B. It also plans the tasks for the user (today, tomorrow, week, month) based on
 

1. location of the user.
2. tasks deadline.
3. task priority.
4. and to be added "road traffic".


# ***More Details:*** #

### **Adding locations** ###

For adding locations, you can use one of the following options to add locations

         1. Type the address of the location with the help of google autocomplete. 

         2. Select the current address of the device as the location address.
              In this case for now the GPS of the device is providing the current
              address. For future releases, Google Places API for current place is
              going to be used in order to retrieve the current address in addition to 
              GPS. 

         3. Select a place in google map and the address of that place will be the
             location address.
You can also provide places Types as an input for locations or you can choose one of the defaults.
You can also view the selected address in a map in case you want to make sure the input address is correct.