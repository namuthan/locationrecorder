package edu.yemen.nabilmuthanna1234.locationrecorder.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.adapter.TasksAdapter;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.OnClickListener;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Location;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Task;

public class LocationDetailActivity extends Activity implements OnClickListener {


    @InjectView(R.id.locationName) TextView mLocationName;
    @InjectView(R.id.addressTextView) TextView mAddress;
    @InjectView(R.id.tasksListForLocation) RecyclerView mTasksForLocation;
    @InjectView(R.id.locationType) TextView mLocationTypeTextView;

    private DatabaseHelper db;
    private TasksAdapter mTaskAdapter;
    private int mLocationId;
    private List<Task> mTasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_detail);
        ButterKnife.inject(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if (!extras.isEmpty()) {
                mLocationId = extras.getInt(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID);
            }
        }
    }

    @OnClick(R.id.add_task_icon) void addNewTask (){

        //Save Location ID
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(LOCATIONRECORDERCONSTANTS.MY_PREFS_NAME, mLocationId);
        editor.commit();

        //Start AddNewTaskActivity
        Intent intent = new Intent(LocationDetailActivity.this, AddNewTaskActivity.class);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID, (int)mLocationId);
        startActivity(intent);
    }

    @OnClick(R.id.editImageView) void editLocation (){
        //Start EditLocation Activity
        Intent intent = new Intent(LocationDetailActivity.this, EditLocationActivity.class);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID, (int)mLocationId);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.EDIT_LOCATION_LOCATION_NAME, mLocationName.getText().toString());
        intent.putExtra(LOCATIONRECORDERCONSTANTS.EDIT_LOCATION_ADDRESS, mAddress.getText().toString());
        startActivity(intent);
    }


    @OnClick(R.id.backArrowImageView) void finshActivity (){
        finish();
    }

    @OnClick(R.id.deleteLocation) void deletLocation (){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure, you want to delete location ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Delete this location
                        //db.deleteLocation(mLocationId);
                        finish();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create();
        builder.show();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (mLocationId <= 0) {
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            mLocationId = sharedPref.getInt(LOCATIONRECORDERCONSTANTS.MY_PREFS_NAME, -1);
        }
        //Get the data from database
        db = new DatabaseHelper(this);

        Location location = db.getLocation(mLocationId);
        mTasks = db.getAllTasks();

        //Sorting mTasks by Titles
        Collections.sort(mTasks);

        mLocationName.setText(location.getName());
        mAddress.setText(location.getAddress());
        //mLocationTypeTextView.setText(db.getLocationType(location.getLocationTypeId()).getLocationTypeName());

        //Set Adapter
        mTaskAdapter = new TasksAdapter(this, mTasks);
        mTaskAdapter.setListType(false);
        mTaskAdapter.setOnClickListener(this);
        mTasksForLocation.setAdapter(mTaskAdapter);
        mTasksForLocation.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onItemClicked(View view, int position) {
        Intent intent = new Intent(this, TaskDetailActivity.class);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_TASK_ID, (int)mTasks.get(position).getId());
        startActivity(intent);
    }
}
