package edu.yemen.nabilmuthanna1234.locationrecorder.activity;

//TODO Make UI better
//TODO Make sure the deadline field is filled before writing it to the data basse
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.fragments.LocationsMultiSelectionList;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Location;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Task;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.TaskLocationAssociation;

public class AddNewTaskActivity extends AppCompatActivity implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    @InjectView(R.id.taskName) EditText mTaskName;
    @InjectView(R.id.taskSummary) EditText mTaskSummary;
    @InjectView(R.id.dynamicLayout) RelativeLayout mDynamicRL;
    @InjectView(R.id.priorityChBx) CheckBox mPriorityChBx;
    @InjectView(R.id.deadLineChBx) CheckBox mDeadLineChBx;
    @InjectView(R.id.priorityRL) RelativeLayout mPriorityRL;
    @InjectView(R.id.deadLineRL) RelativeLayout mDeadLineRL;
    @InjectView (R.id.showOtherFieldsBtn) Button mShowOtherFieldsBtn;
    @InjectView(R.id.taskDescriptionEditText) EditText mTaskDescription;

    private DatabaseHelper db;
    private int mLocationId;
    private ArrayList<String> mSelections;
    private ArrayList<Location> mLocationSelections;

    private View mPriorityLayout;
    private View mDeadLineLayout;

    private TextView mDeadLineTextview = null;
    private String mCalender = "";
    private String mTime = "";
    private String mDeadLine = "";
    private float mPriority = 0;
    private boolean mDynamicRLIsHidden;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);
        ButterKnife.inject(this);
        mLocationId = -1;
        mLocationSelections = new ArrayList<Location>();
        mDynamicRLIsHidden = true;
        mDynamicRL.setVisibility(View.INVISIBLE);

        Bundle extras = getIntent().getExtras();
        mLocationId = extras.getInt(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID);

        db = new DatabaseHelper(getApplicationContext());

        SharedPreferences.Editor editor = getSharedPreferences(LOCATIONRECORDERCONSTANTS.MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID, mLocationId);
        editor.commit();

        mPriorityLayout = getLayoutInflater().inflate(R.layout.custom_task_priority, null);
        mDeadLineLayout = getLayoutInflater().inflate(R.layout.custome_task_deadline, null);


        mPriorityChBx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                handleOnPriorityChChange(b);
            }
        });

        mDeadLineChBx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                handleOnDeadLineChChange(b);
            }
        });
    }

    private void handleOnDeadLineChChange(boolean b) {
        if(b == true && mDeadLineChBx.isChecked()){
            mDeadLineRL.addView(mDeadLineLayout);
            mDeadLineChBx.setVisibility(View.INVISIBLE);
            ImageView imageView = (ImageView) mDeadLineRL.findViewById(R.id.delteTaskDeadLine);
            mDeadLineTextview = (TextView) mDeadLineRL.findViewById(R.id.deadlineText);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDeadLineRL.removeView(mDeadLineLayout);
                    mDeadLineChBx.setChecked(false);
                    mDeadLineChBx.setVisibility(View.VISIBLE);
                }
            });

            //Fire up the date and timep picker
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    AddNewTaskActivity.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getFragmentManager(), "Datepickerdialog");
        }
    }

    private void handleOnPriorityChChange(boolean b) {
        if(b == true && mPriorityChBx.isChecked()){
            mPriorityRL.addView(mPriorityLayout);
            mPriorityChBx.setVisibility(View.INVISIBLE);
            ImageView imageView = (ImageView) mPriorityRL.findViewById(R.id.delteTaskPriority);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPriorityRL.removeView(mPriorityLayout);
                    mPriorityChBx.setChecked(false);
                    mPriorityChBx.setVisibility(View.VISIBLE);

                }
            });

            RatingBar ratingBar = (RatingBar) mPriorityRL.findViewById(R.id.ratingBar);
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    String prio = "Priority is: " + v;
                    Toast toast = Toast.makeText(AddNewTaskActivity.this, prio, Toast.LENGTH_LONG);
                    toast.show();
                    mPriority = v;
                }
            });
        }
    }


    @OnClick(R.id.addTaskButton) void submit() {

        int duration = Toast.LENGTH_SHORT;
        Context context = getApplicationContext();

        if (mTaskName.getText().toString().equals("") || mTaskSummary.getText().toString().equals("")){
            Toast toast = Toast.makeText(context, "Make sure the name and Summary fields are filled"  , duration);
            toast.show();
            return;
        }

        Task task = getTaskObjectFromUI();



        if(!checkifTaskExists(task)) {
            long taskId = db.createTask(task);

            //Add the corresponding TaskLocationAssociation
            for(int i = 0; i<mLocationSelections.size(); i++){
                long id = db.createLocationTaskAssociation(new TaskLocationAssociation(mLocationSelections.get(i).getId(), (int)taskId));
                TaskLocationAssociation tmp =  db.getTaskLocationAssociation(id);

                Toast.makeText(this, "LocationTaskAss. is saved with the following inf" + "Location Id " +
                    +tmp.getLocationId() + "\n Task ID "+   tmp.getTaskId(), Toast.LENGTH_LONG);
            }

            Toast toast = Toast.makeText(context, "Task Saved :) ", duration);
            toast.show();
            Intent intent = new Intent(this, TaskDetailActivity.class);
            intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_TASK_ID, (int) taskId);
            startActivity(intent);
            finish();
        }
        else{
            Toast toast = Toast.makeText(context, "Task with this name already exists :(", duration);
            toast.show();
        }

        db.closeDB();
    }

    private Task getTaskObjectFromUI() {

        Task task = new Task();

        task.setTitle(mTaskName.getText().toString());
        task.setSummary(mTaskSummary.getText().toString());
        task.setDescription(mTaskDescription.getText().toString());

        if(!mDynamicRLIsHidden){
            task.setPriority(mPriority);
            task.setDeadLine(mDeadLine);
        } else{
            task.setPriority(LOCATIONRECORDERCONSTANTS.TASK_NO_PRIORITY_ADDED);
            task.setDeadLine(LOCATIONRECORDERCONSTANTS.TASK_NO_DEADLINE_ADDED);
        }

        task.setLevelOfImportance(LOCATIONRECORDERCONSTANTS.NORMAL);
        task.setLevelOfProgress(LOCATIONRECORDERCONSTANTS.TASK_PROGRESS_ADDED);

        return task;
    }

    public boolean checkifTaskExists(Task task) {
        List<Task> tasks = db.getAllTasksForLocation(mLocationId);

        for(int i = 0; i<tasks.size(); i++){
            if(tasks.get(i).getTitle().toLowerCase().compareTo(task.getTitle().toLowerCase()) == 0)
            {
                return true;
            }
        }
        return false;
    }

    public boolean checkifLocationIdExists (int locationId){

        List<Location> locations = db.getAllLocations();
        for(int i = 0; i<locations.size(); i++){
            if(locations.get(i).getId() == locationId){
                return true;
            }
        }
        return false;
    }

    @OnClick (R.id.cancelAddTaskBtn) void Cancel (){
        finish();
    }

    @OnClick (R.id.addLocationsBtn)  void addTasksLocation (){
        //Show multiSelection Dialog of the available locations
        LocationsMultiSelectionList multiSelectionList = new LocationsMultiSelectionList();
        multiSelectionList.show(getSupportFragmentManager(), "multiSelectLocations");
    }

    @OnClick (R.id.showOtherFieldsBtn) void addOtherFields (){
        if(mDynamicRLIsHidden){

            mDynamicRL.setVisibility(View.VISIBLE);
            mDynamicRLIsHidden = false;
            mShowOtherFieldsBtn.setText("Hide Other Fields");
        } else{
            mDynamicRL.setVisibility(View.INVISIBLE);
            mDynamicRLIsHidden = true;
            mShowOtherFieldsBtn.setText("Show Other Fields");
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth){
        monthOfYear += 1;
        mCalender = ""+monthOfYear+"/"+dayOfMonth+"/"+year;


        Toast toast = Toast.makeText(this, mCalender, Toast.LENGTH_LONG);
        toast.show();

        //Start time dialog
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                AddNewTaskActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );

        tpd.setThemeDark(true);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog");

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {

        mTime = getTimein12(hourOfDay, minute);
        Toast toast = Toast.makeText(this, mTime, Toast.LENGTH_LONG);
        toast.show();

        mDeadLine = mCalender + " " +mTime;
        if(mDeadLineTextview != null){
            mDeadLineTextview.setText(mDeadLine);
        }

    }

    public String getTimein12(int hourOfDay, int minute){

        String time = "";
        String AMPM ="";

        String hour = "";
        if(hourOfDay < 10){
            hour += "0";
        }

        if(hourOfDay > 12){
            hour += (hourOfDay - 12);
            AMPM = "PM";
        } else{
            hour += hourOfDay;
            AMPM = "AM";
        }

        time = hour + ":" + minute + " " + AMPM;

        return time;
    }

    public void setmSelections (ArrayList<String> selections){

        boolean ratingBar = false;
        String arr[] = getResources().getStringArray(R.array.task_field_selections);
        mSelections = selections;
        String detail = "SELECTIONS";
        for(int i = 0; i<mSelections.size(); i++){
            detail += "\n" + mSelections.get(i);
        }

        Toast toast = Toast.makeText(this, detail, Toast.LENGTH_LONG);
        toast.show();
    }

    public void setLocationSelections (ArrayList<Location> locationSelections){
        mLocationSelections = locationSelections;
    }
}
