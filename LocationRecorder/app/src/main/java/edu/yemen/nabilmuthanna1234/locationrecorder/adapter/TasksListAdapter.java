package edu.yemen.nabilmuthanna1234.locationrecorder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.OnClickListener;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.TasksList;
/**
 * Created by nabil on 2015-06-22.
 */


public class TasksListAdapter extends RecyclerView.Adapter< RecyclerView.ViewHolder > {

    private List<TasksList> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context mContext;
    private List<Integer> mSelectedRows = new ArrayList<Integer>();
    private DatabaseHelper db;

    boolean mAllTaks;
    public OnClickListener mOnClickListener;

    public void setOnClickListener (OnClickListener onClickListener){
        this.mOnClickListener = onClickListener;
    }

    public TasksListAdapter(Context context, List<TasksList> data){
        db = new DatabaseHelper(context);
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        mAllTaks = true;
    }

    public void setListType (boolean allTasks){
        this.mAllTaks = allTasks;
    }

    @Override
    public int getItemCount () {
        return mData.size ();
    }

    @Override
    public int getItemViewType ( int position ) {
        int viewType;
        if (mData.get(position).getType () ==
                LOCATIONRECORDERCONSTANTS.TASK_LIST_HEADER) {
            viewType = LOCATIONRECORDERCONSTANTS.TASK_LIST_HEADER;
        } else {
            viewType = LOCATIONRECORDERCONSTANTS.TASK_LIST_BODY;
        }
        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder ( ViewGroup viewGroup, int viewType ) {
        LayoutInflater mInflater = LayoutInflater.from (viewGroup.getContext ());

        if(viewType == LOCATIONRECORDERCONSTANTS.TASK_LIST_HEADER){
            ViewGroup vHeader = ( ViewGroup ) mInflater.inflate (R.layout.task_list_header, viewGroup, false);
            HeaderTaskHolder1 vhHeader = new HeaderTaskHolder1 (vHeader);
            return vhHeader;
        } else {
            ViewGroup vBody = (ViewGroup) mInflater.inflate (R.layout.custom_task_item_layout, viewGroup, false);
            TaskHolder1 vhBody = new TaskHolder1 (vBody);
            return vhBody;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TasksList current = mData.get(position);
        if(current.getType() == LOCATIONRECORDERCONSTANTS.TASK_LIST_BODY) {
            TaskHolder1 itemHolder = (TaskHolder1) holder;
            itemHolder.mTitle.setText(current.getTask().getTitle());
            if (mAllTaks) {
                itemHolder.mLine.setVisibility(View.INVISIBLE);
                itemHolder.mLocationName.setText(current.getTask().getSummary());
            } else {
                itemHolder.mLocationName.setText(current.getTask().getSummary());
            }
            itemHolder.mOnClickListener = this.mOnClickListener;
        }

        else{
            HeaderTaskHolder1 itemHolder = (HeaderTaskHolder1) holder;
            itemHolder.mLocationName.setText(current.getLocation().getName());
            itemHolder.mNumberOfTasks.setText(db.getAllTasksForLocation(current.getLocation().getId()).size() + " Tasks");
            if(position == 0){
                itemHolder.mLine.setVisibility(View.INVISIBLE);
            } else{
                itemHolder.mLine.setVisibility(View.VISIBLE);
            }
        }
    }
}



//ViewHolders

class MainViewHolder extends RecyclerView.ViewHolder {

     public MainViewHolder ( View itemView ) {
        super ( itemView );
    }
}

class  TaskHolder1 extends MainViewHolder implements View.OnClickListener {
    TextView mTitle;
    TextView mLocationName;
    View mLine;
    TextView mSummary;
    ImageView taskIcon;
    public OnClickListener mOnClickListener;


    public TaskHolder1(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mTitle = (TextView) itemView.findViewById(R.id.taskTitle);
        mLocationName = (TextView) itemView.findViewById(R.id.locationNameForTask);
        mLine = (View) itemView.findViewById(R.id.line);
        taskIcon = (ImageView) itemView.findViewById(R.id.taskIcon);
    }

    @Override
    public void onClick(View view) {
        if (mOnClickListener != null) {
            mOnClickListener.onItemClicked(view, getPosition());
        }

    }
}



class  HeaderTaskHolder1 extends MainViewHolder {

    TextView mLocationName;
    View mLine;
    TextView mNumberOfTasks;


    public HeaderTaskHolder1(View itemView) {
        super(itemView);
        itemView.setClickable(true);
        mLocationName = (TextView) itemView.findViewById(R.id.headerLocationName);
        mLine = (View) itemView.findViewById(R.id.line);
        mNumberOfTasks = (TextView) itemView.findViewById(R.id.numberOfTasksField);
    }

}


