package edu.yemen.nabilmuthanna1234.locationrecorder.model;

/**
 * Created by nabil on 2015-06-22.
 */
public class TasksList {

    private Location mLocation;
    private Task mTask;
    private int mType;


    public TasksList (Location location, Task task, int type){
        this.mLocation = new Location(location);
        this.mTask = new Task(task);
        this.mType = type;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location mLocation) {
        this.mLocation = mLocation;
    }

    public Task getTask() {
        return mTask;
    }

    public void setTask(Task mTask) {
        this.mTask = mTask;
    }

    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

}
