package edu.yemen.nabilmuthanna1234.locationrecorder.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.activity.TaskDetailActivity;
import edu.yemen.nabilmuthanna1234.locationrecorder.adapter.TasksAdapter;
import edu.yemen.nabilmuthanna1234.locationrecorder.adapter.TasksListAdapter;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.OnClickListener;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Location;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Task;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.TasksList;


public class TasksFragment extends Fragment implements OnClickListener {

    private RecyclerView mRecyclerView;
    private TasksAdapter mTaskAdapter;
    private TasksListAdapter mTasksListAdapter;
    List<Task> mTasks;
    List<TasksList> mTasksList;
    List<Location> mLocations;

    // Database Helper
    DatabaseHelper db;


    RelativeLayout relativeLayout;
    public TasksFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tasks, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.tasksList);
        setupDatabaseAndList();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupDatabaseAndList();
    }

    @Override
    public void onItemClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), TaskDetailActivity.class);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_TASK_ID, (int) mTasksList.get(position).getTask().getId());
        startActivity(intent);
    }

    public void setupDatabaseAndList (){
        setUpListforAdapter();
    }

    private void setUpListforAdapter() {

        mTasksList = new ArrayList<TasksList>();
        db = new DatabaseHelper(this.getActivity());
        List<Location> locations = db.getAllLocations();
        Collections.sort(locations);

        for(int i = 0; i<locations.size(); i++){
            TasksList tasksList = new TasksList(locations.get(i), null,
                    LOCATIONRECORDERCONSTANTS.TASK_LIST_HEADER);
            mTasksList.add(tasksList);

            List<Task> tasks = new ArrayList<Task>();
            tasks = db.getAllTasksForLocation(locations.get(i).getId());
            Collections.sort(tasks);

            for(int j = 0; j<tasks.size(); j++){
                TasksList tasksList1 = new TasksList(null, tasks.get(j),
                        LOCATIONRECORDERCONSTANTS.TASK_LIST_BODY);
                mTasksList.add(tasksList1);
            }
        }

        //Set Adapter
        mTasksListAdapter = new TasksListAdapter(getActivity(), mTasksList);
        mTasksListAdapter.setOnClickListener(this);
        mRecyclerView.setAdapter(mTasksListAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }
}
