package edu.yemen.nabilmuthanna1234.locationrecorder.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import edu.yemen.nabilmuthanna1234.locationrecorder.model.Location;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.LocationType;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Task;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.TaskLocationAssociation;

/**
 * Created by nabil on 2015-06-18.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    //Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 12;

    //Database Name
    private static final String DATABASE_NAME = "locationsRecorder";

    //Table Names
    private static final String TABLE_LOCATION = "locations";
    private static final String TABLE_TASK = "tasks";
    private static final String TABLE_LOCATION_TYPES = "location_types";
    private static final String TABLE_LTASSOCIATION = "location_to_tasks";


    //Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_CREATED_AT = "created_at";

    //Location Table - column names
    private static final String KEY_LOCATION_NAME = "name";
    private static final String KEY_LOCATION_ADDRESS = "address";
    private static final String KEY_LOCATION_LATITUDE = "latitude";
    private static final String KEY_LOCATION_LONGTITUDE = "longtitude";
    private static final String KEY_LOCATION_LEVEL_OF_IMPORTANCE = "location_level_of_importance";
    private static final String KEY_LOCATION_LOCATION_TYPE_ID = "location_type_id";

    //Task Table - column names
    private static final String KEY_TASK_TITLE = "title";
    private static final String KEY_TASK_SUMMARY = "summary";
    private static final String KEY_TASK_DESCRIPTION = "description";
    private static final String KEY_TASK_PRIORITY = "task_priority";
    private static final String KEY_TASK_LEVEL_OF_PROGRESS = "level_of_progress";
    private static final String KEY_TASK_DEADLINE = "task_deadline";
    private static final String KEY_TASK_LEVEL_OF_IMPORTANCE = "task_level_of_importance";

    //Location Types
    private static final String KEY_LOCATION_TYPE_TYPE = "location_type";
    private static final String KEY_LOCATION_TYPE_RANGE = "location_type_range";
    private static final String KEY_LOCATION_TYPE_UNIT = "location_unit";

    //Location to task association
    private static final String KEY_LTASSOCIATION_LOCATION_ID = "location_id_index";
    private static final String KEY_LTASSOCIATION_TASK_ID = "task_id_index";

    /** Table Create Statements  */

    // Locations table create statement
    private static final String CREATE_TABLE_LOCATION = "CREATE TABLE "
            + TABLE_LOCATION + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_LOCATION_NAME + " TEXT,"
            + KEY_LOCATION_ADDRESS + " TEXT,"
            + KEY_LOCATION_LATITUDE + " TEXT,"
            + KEY_LOCATION_LEVEL_OF_IMPORTANCE + " INTEGER,"
            + KEY_LOCATION_LONGTITUDE + " TEXT,"
            + KEY_LOCATION_LOCATION_TYPE_ID + " INTEGER,"
            + KEY_CREATED_AT + " DATETIME" + ")";

    // Tasks table create statement
    private static final String CREATE_TABLE_TASK = "CREATE TABLE "
            + TABLE_TASK + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_TASK_TITLE + " TEXT,"
            + KEY_TASK_LEVEL_OF_IMPORTANCE + " INTEGER,"
            + KEY_TASK_LEVEL_OF_PROGRESS + " INTEGER,"
            + KEY_TASK_SUMMARY + " TEXT,"
            + KEY_TASK_DESCRIPTION + " TEXT,"
            + KEY_TASK_PRIORITY +" REAL,"
            + KEY_TASK_DEADLINE + " TEXT,"
            + KEY_CREATED_AT + " DATETIME" + ")";


    private static final String CREATE_TABLE_LTASSOCIATION = "CREATE TABLE "
            + TABLE_LTASSOCIATION + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_LTASSOCIATION_LOCATION_ID + " INTEGER,"
            + KEY_LTASSOCIATION_TASK_ID + " INTEGER,"
            + KEY_CREATED_AT + " DATETIME" + ")";


    // LocationTypes Table
    private static final String CREATE_TABLE_LOCATION_TYPES = "CREATE TABLE "
            + TABLE_LOCATION_TYPES + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_LOCATION_TYPE_TYPE + " TEXT,"
            + KEY_LOCATION_TYPE_UNIT + " TEXT,"
            + KEY_LOCATION_TYPE_RANGE + " INTEGER,"
            + KEY_CREATED_AT + " DATETIME" + ")";



    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {

        // creating required tables
        db.execSQL(CREATE_TABLE_LOCATION);
        db.execSQL(CREATE_TABLE_TASK);
        db.execSQL(CREATE_TABLE_LOCATION_TYPES);
        db.execSQL(CREATE_TABLE_LTASSOCIATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1)
    {

        System.out.println("Upgrading Database table..");
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION_TYPES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LTASSOCIATION);

        // create new tables
        onCreate(db);
    }



    /*--------------------------------- CREATE ---------------------------------*/

    public long createLocation(Location location)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOCATION_NAME, location.getName());
        values.put(KEY_LOCATION_ADDRESS, location.getAddress());
        values.put(KEY_LOCATION_LATITUDE, location.getLatitude());
        values.put(KEY_LOCATION_LONGTITUDE, location.getLongtitude());
        values.put(KEY_LOCATION_LEVEL_OF_IMPORTANCE, location.getLevelOfImportance());
        values.put(KEY_LOCATION_LOCATION_TYPE_ID, location.getLocationTypeId());
        values.put(KEY_CREATED_AT, getDateTime());

        // insert row
        long locationId = db.insert(TABLE_LOCATION, null, values);

        return locationId;
    }

    public long createTask(Task task)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TASK_TITLE, task.getTitle());
        values.put(KEY_TASK_SUMMARY, task.getSummary());
        values.put(KEY_TASK_DESCRIPTION, task.getDescription());
        values.put(KEY_TASK_DEADLINE, task.getDeadLine());
        values.put(KEY_TASK_PRIORITY, task.getPriority());
        values.put(KEY_TASK_LEVEL_OF_PROGRESS, task.getLevelOfProgress());
        values.put(KEY_TASK_LEVEL_OF_IMPORTANCE, task.getLevelOfImportance());
        values.put(KEY_CREATED_AT, getDateTime());

        // insert row
        long taskId = db.insert(TABLE_TASK, null, values);

        return taskId;
    }

    public long createLocationType (LocationType locationType)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOCATION_TYPE_TYPE, locationType.getLocationTypeName());
        values.put(KEY_LOCATION_TYPE_RANGE, locationType.getRange());
        values.put(KEY_LOCATION_TYPE_UNIT, locationType.getUnitType());
        values.put(KEY_CREATED_AT, getDateTime());

        // insert row
        long locationTypeId = db.insert(TABLE_LOCATION_TYPES, null, values);

        return locationTypeId;
    }

    public long createLocationTaskAssociation (TaskLocationAssociation taskLocationAssociation)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LTASSOCIATION_TASK_ID, taskLocationAssociation.getTaskId());
        values.put(KEY_LTASSOCIATION_LOCATION_ID, taskLocationAssociation.getLocationId());
        values.put(KEY_CREATED_AT, getDateTime());

        long taskLocationAssociationId = db.insert(TABLE_LTASSOCIATION, null, values);

        return taskLocationAssociationId;
    }



    /*--------------------------------- READ ---------------------------------*/

    public Location getLocation(long locationId)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION + " WHERE "
                + KEY_ID + " = " + locationId;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null)
            c.moveToFirst();

        Location location = new Location();
        location.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        location.setName(c.getString(c.getColumnIndex(KEY_LOCATION_NAME)));
        location.setAddress(c.getString(c.getColumnIndex(KEY_LOCATION_ADDRESS)));
        location.setLatitude(c.getDouble(c.getColumnIndex(KEY_LOCATION_LATITUDE)));
        location.setLongtitude(c.getDouble(c.getColumnIndex(KEY_LOCATION_LONGTITUDE)));
        location.setLevelOfImportance(c.getInt(c.getColumnIndex(KEY_LOCATION_LEVEL_OF_IMPORTANCE)));
        location.setLocationTypeId(c.getInt(c.getColumnIndex(KEY_LOCATION_LOCATION_TYPE_ID)));
        location.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));

        return location;
    }

    public Task getTask (long taskId){
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_TASK + " WHERE "
                + KEY_ID + " = " + taskId;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null)
            c.moveToFirst();

        Task task = new Task();
        task.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        task.setTitle(c.getString(c.getColumnIndex(KEY_TASK_TITLE)));
        task.setSummary(c.getString(c.getColumnIndex(KEY_TASK_SUMMARY)));
        task.setDescription(c.getString(c.getColumnIndex(KEY_TASK_DESCRIPTION)));
        task.setLevelOfImportance(c.getInt(c.getColumnIndex(KEY_TASK_LEVEL_OF_IMPORTANCE)));
        task.setDeadLine(c.getString(c.getColumnIndex(KEY_TASK_DEADLINE)));
        task.setPriority(c.getFloat(c.getColumnIndex(KEY_TASK_PRIORITY)));
        task.setLevelOfProgress(c.getInt(c.getColumnIndex(KEY_TASK_LEVEL_OF_PROGRESS)));
        task.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));

        return task;
    }

    public LocationType getLocationType (long locationTypeId)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION_TYPES + " WHERE "
                + KEY_ID + " = " + locationTypeId;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        return getLocationTypeObject(c);
    }

    public TaskLocationAssociation getTaskLocationAssociation (long LTId)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_LTASSOCIATION + " WHERE "
                + KEY_ID + " = " + LTId;
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        TaskLocationAssociation taskLocationAssociation = new TaskLocationAssociation();
        taskLocationAssociation.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        taskLocationAssociation.setLocationId(c.getInt(c.getColumnIndex(KEY_LTASSOCIATION_LOCATION_ID)));
        taskLocationAssociation.setTaskId(c.getInt(c.getColumnIndex(KEY_LTASSOCIATION_TASK_ID)));
        taskLocationAssociation.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));

        return taskLocationAssociation;
    }

    public List<Location> getAllLocations()
    {
        List<Location> locations = new ArrayList<Location>();

        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                locations.add(getocationObject(c));
            } while (c.moveToNext());
        }

        return locations;
    }

    public List<Task> getAllTasks ()
    {
        List<Task> tasks = new ArrayList<Task>();

       String selectQuery = "SELECT  * FROM " + TABLE_TASK;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                tasks.add(getTaskObject(c));
            } while (c.moveToNext());
        }
        return tasks;
    }

    public ArrayList<LocationType> getAllLocationTypes ()
    {
        ArrayList<LocationType> locationTypes = new ArrayList<LocationType>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION_TYPES;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                locationTypes.add(getLocationTypeObject(c));
            } while (c.moveToNext());
        }

        return locationTypes;
    }

    public ArrayList<TaskLocationAssociation> getAllTaskLocationAssociation ()
    {

        ArrayList<TaskLocationAssociation> TaskLocationTypes = new ArrayList<TaskLocationAssociation>();
        String selectQuery = "SELECT  * FROM " + TABLE_LTASSOCIATION;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                // adding to tasks list
                TaskLocationTypes.add(getTaskLocationAssociation(c));
            } while (c.moveToNext());
        }
        return TaskLocationTypes;
    }

    public ArrayList<Location> getAllLocationsForTask (long taskId)
    {
        ArrayList<Location> locations = new ArrayList<Location>();
        String selectQuery = "SELECT  * FROM " + TABLE_LTASSOCIATION + " WHERE "
                + KEY_LTASSOCIATION_TASK_ID + " = " + taskId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);


        if (c.moveToFirst()) {
            do {
                locations.add(getLocation(c.getInt(c.getColumnIndex(KEY_LTASSOCIATION_LOCATION_ID))));
            } while (c.moveToNext());

        }

        return locations;
    }

    public ArrayList<Task> getAllTasksForLocation (long locationId)
    {

        ArrayList<Task> tasks = new ArrayList<Task>();
        String selectQuery = "SELECT  * FROM " + TABLE_LTASSOCIATION + " WHERE "
                + KEY_LTASSOCIATION_LOCATION_ID + " = " + locationId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);


        if (c.moveToFirst()) {
            do {
                tasks.add(getTask(c.getInt(c.getColumnIndex(KEY_LTASSOCIATION_TASK_ID))));
            } while (c.moveToNext());

        }

        return tasks;
    }




    /*--------------------------------- UPDATE ---------------------------------*/

   public int updateLocation(Location location)
   {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOCATION_NAME, location.getName());
        values.put(KEY_LOCATION_ADDRESS, location.getAddress());
        values.put(KEY_LOCATION_LATITUDE, location.getLatitude());
        values.put(KEY_LOCATION_LONGTITUDE, location.getLongtitude());
        values.put(KEY_CREATED_AT, getDateTime());

        // updating row
        return db.update(TABLE_LOCATION, values, KEY_ID + " = ?",
                new String[] { String.valueOf(location.getId()) });
    }

   public int updateTask(Task task)
   {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(KEY_TASK_LOCATION_ID, task.getLocationId());
        values.put(KEY_TASK_DESCRIPTION, task.getDescription());
        values.put(KEY_TASK_TITLE, task.getTitle());
        values.put(KEY_TASK_SUMMARY, task.getSummary());
        values.put(KEY_CREATED_AT, getDateTime());

        // updating row
        return db.update(TABLE_TASK, values, KEY_ID + " = ?",
                new String[] { String.valueOf(task.getId()) });
    }





    /*--------------------------------- DELETE ---------------------------------*/

    public void deleteLocation(long id)
    {

        deletAllTasksForLocation(id);

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LOCATION, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    public void deleteAllLocations()
    {
        List<Location> locations = getAllLocations();

        for(int i = 0; i<locations.size(); i++){
            deleteLocation(locations.get(i).getId());
        }
    }

    public void deleteAllTasks()
    {
        List<Task> tasks = getAllTasks();

        for(int i = 0; i<tasks.size(); i++){
            deleteTask(tasks.get(i).getId());
        }
    }

    public void deletAllTasksForLocation (long LocationId)
    {
        /*List<Task> tasks = getAllTasksForLocation(LocationId);
        for(int i = 0; i<tasks.size(); i++ ){
            deleteTask(tasks.get(i).getId());
        }
        */
    }

    public void deleteTask(long id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TASK, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
    }



    /*--------------------------------- HELPERS ---------------------------------*/

    private  Location getocationObject (Cursor c)
    {
        Location location = new Location();
        location.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        location.setName(c.getString(c.getColumnIndex(KEY_LOCATION_NAME)));
        location.setAddress(c.getString(c.getColumnIndex(KEY_LOCATION_ADDRESS)));
        location.setLatitude(c.getDouble(c.getColumnIndex(KEY_LOCATION_LATITUDE)));
        location.setLongtitude(c.getDouble(c.getColumnIndex(KEY_LOCATION_LONGTITUDE)));
        location.setLevelOfImportance(c.getInt(c.getColumnIndex(KEY_LOCATION_LEVEL_OF_IMPORTANCE)));
        location.setLocationTypeId(c.getInt(c.getColumnIndex(KEY_LOCATION_LOCATION_TYPE_ID)));
        location.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));

        return location;
    }

    private Task getTaskObject (Cursor c)
    {
        Task task = new Task();
        task.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        task.setTitle(c.getString(c.getColumnIndex(KEY_TASK_TITLE)));
        task.setSummary(c.getString(c.getColumnIndex(KEY_TASK_SUMMARY)));
        task.setDescription(c.getString(c.getColumnIndex(KEY_TASK_DESCRIPTION)));
        task.setLevelOfImportance(c.getInt(c.getColumnIndex(KEY_TASK_LEVEL_OF_IMPORTANCE)));
        task.setDeadLine(c.getString(c.getColumnIndex(KEY_TASK_DEADLINE)));
        task.setPriority(c.getFloat(c.getColumnIndex(KEY_TASK_PRIORITY)));
        task.setLevelOfProgress(c.getInt(c.getColumnIndex(KEY_TASK_LEVEL_OF_PROGRESS)));
        task.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));


        return task;
    }

    private LocationType getLocationTypeObject (Cursor c)
    {
        LocationType locationType = new LocationType();
        locationType.setLocationTypeName(c.getString(c.getColumnIndex(KEY_LOCATION_TYPE_TYPE)));
        locationType.setRange(c.getInt(c.getColumnIndex(KEY_LOCATION_TYPE_RANGE)));
        locationType.setUnitType(c.getString(c.getColumnIndex(KEY_LOCATION_TYPE_UNIT)));
        locationType.setID(c.getInt(c.getColumnIndex(KEY_ID)));

        return locationType;
    }

    private TaskLocationAssociation getTaskLocationAssociation (Cursor c)
    {
        TaskLocationAssociation taskLocationAssociation = new TaskLocationAssociation();
        taskLocationAssociation.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        taskLocationAssociation.setLocationId(c.getInt(c.getColumnIndex(KEY_LTASSOCIATION_LOCATION_ID)));
        taskLocationAssociation.setTaskId(c.getInt(c.getColumnIndex(KEY_LTASSOCIATION_TASK_ID)));
        taskLocationAssociation.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));

        return taskLocationAssociation;
    }







    public void closeDB()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    private String getDateTime()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
