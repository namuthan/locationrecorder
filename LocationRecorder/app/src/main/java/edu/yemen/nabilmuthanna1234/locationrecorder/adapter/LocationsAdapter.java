package edu.yemen.nabilmuthanna1234.locationrecorder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.OnClickListener;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Location;

/**
 * Created by nabil on 2015-06-20.
 */
public class LocationsAdapter extends RecyclerView.Adapter<ItemHolder> {

    List<Location> mData= Collections.emptyList();
    private LayoutInflater mInflater;
    private Context mContext;
    public OnClickListener mOnClickListener;

    public LocationsAdapter(Context context, List<Location> data){

        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    public void setOnClickListener (OnClickListener onClickListener){
        this.mOnClickListener = onClickListener;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = mInflater.inflate(R.layout.custom_location_item_layout, parent,false);
            ItemHolder holder=new ItemHolder(view);
            return holder;
    }


    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {


        ItemHolder itemHolder= (ItemHolder) holder;
        Location current = mData.get(position);
        itemHolder.mName.setText(current.getName());
        holder.mOnClickListener = this.mOnClickListener;
        itemHolder.mAddress.setText(current.getAddress());
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }


}

 class  ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView mName;
    TextView mAddress;
     public OnClickListener mOnClickListener;

    public ItemHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mName = (TextView) itemView.findViewById(R.id.locationName);
        mAddress = (TextView) itemView.findViewById(R.id.addressTextView);
    }

     @Override
     public void onClick(View view) {
         if(mOnClickListener != null){
             mOnClickListener.onItemClicked(view, getPosition());
         }

     }
 }
