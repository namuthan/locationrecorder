package edu.yemen.nabilmuthanna1234.locationrecorder.fragments;


import android.app.Activity;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.activity.LocationDetailActivity;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;


public class HomeFragment extends Fragment{

    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private String mProvider;
    private Marker mNow;

    private Button btn;
    private Button location_details_btn;
    private Button delete_tasks_btn;
    private Button upgradeDbBtn;
    // Database Helper
    DatabaseHelper db;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db = new DatabaseHelper(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        btn = (Button) rootView.findViewById(R.id.dlt_locations_btn);
        location_details_btn = (Button) rootView.findViewById(R.id.show_location_details);
        delete_tasks_btn = (Button) rootView.findViewById(R.id.dlt_tasks_btn);
        upgradeDbBtn = (Button) rootView.findViewById(R.id.upgradeDbBtn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.deleteAllLocations();
            }
        });

        location_details_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LocationDetailActivity.class);
                intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID, (int)1);
                startActivity(intent);
            }
        });

        delete_tasks_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               db.deleteAllTasks();
            }
        });

        upgradeDbBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //db.up();
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
