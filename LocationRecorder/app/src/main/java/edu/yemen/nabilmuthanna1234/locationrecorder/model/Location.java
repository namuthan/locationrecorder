package edu.yemen.nabilmuthanna1234.locationrecorder.model;

import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;

/**
 * Created by nabil on 2015-06-18.
 */
public class Location  implements Comparable, Cloneable {

    //private variables
    private int mId;
    private String mName;
    private String mAddress;
    private double mLatitude;
    private double mLongtitude;
    private int mLevelOfImportance;
    private int mLocationTypeId;
    private String mCreatedAt;

    //Empty Constructor
    public Location() {}

    //Copy Constructor
    public Location(Location anotherLocation){
        if(anotherLocation != null) {
            this.mId = anotherLocation.getId();
            this.mName = anotherLocation.getName();
            this.mAddress = anotherLocation.getAddress();
            this.mLatitude = anotherLocation.getLatitude();
            this.mLongtitude = anotherLocation.getLongtitude();
            this.mCreatedAt = anotherLocation.getCreatedAt();
            this.mLevelOfImportance = anotherLocation.getLevelOfImportance();
        }
    }

    public Location(int id, String name, double lat, double lon, String address) {
        this.mId = id;
        this.mName = name;
        this.mLatitude = lat;
        this.mLongtitude = lon;
        this.mAddress = address;
        this.mLevelOfImportance = LOCATIONRECORDERCONSTANTS.NORMAL;
    }

    public Location(String name, double lat, double lon, String address) {
        this.mName = name;
        this.mLatitude = lat;
        this.mLongtitude = lon;
        this.mAddress = address;
        this.mLevelOfImportance = LOCATIONRECORDERCONSTANTS.NORMAL;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }


    /**
     * Getters and Setters
     */
    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongtitude() {
        return mLongtitude;
    }

    public void setLongtitude(double mLongtitude) {
        this.mLongtitude = mLongtitude;
    }

    public int getLevelOfImportance() {
        return mLevelOfImportance;
    }

    public void setLevelOfImportance(int mLevelOfImportance) {
        this.mLevelOfImportance = mLevelOfImportance;
    }

    public int getLocationTypeId() {
        return mLocationTypeId;
    }

    public void setLocationTypeId(int mLocationTypeId) {
        this.mLocationTypeId = mLocationTypeId;
    }
    /**
     * Helpers Functions
     */
    public int compareTo(Object anotherLocation) throws ClassCastException {
        if (!(anotherLocation instanceof Location))
            throw new ClassCastException("A Location object expected.");
        Location location  = (Location) anotherLocation;

        return this.getName().compareToIgnoreCase(((Location) anotherLocation).getName());
    }
}