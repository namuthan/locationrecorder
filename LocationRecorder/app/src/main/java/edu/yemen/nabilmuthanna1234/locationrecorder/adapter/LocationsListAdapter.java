package edu.yemen.nabilmuthanna1234.locationrecorder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.OnClickListener;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.LocationList;
/**
 * Created by nabil on 2015-06-22.
 */


public class LocationsListAdapter extends RecyclerView.Adapter< RecyclerView.ViewHolder > {

    private List<LocationList> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context mContext;


    public OnClickListener mOnClickListener;

    public void setOnClickListener (OnClickListener onClickListener){
        this.mOnClickListener = onClickListener;
    }

    public LocationsListAdapter(Context context, List<LocationList> data){
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }


    @Override
    public int getItemCount () {
        return mData.size ();
    }

    @Override
    public int getItemViewType ( int position ) {
        int viewType;

        if (mData.get(position).getTyp() ==
                LOCATIONRECORDERCONSTANTS.LOCATION_LIST_HEADER) {
            viewType = LOCATIONRECORDERCONSTANTS.LOCATION_LIST_HEADER;
        } else {
            viewType = LOCATIONRECORDERCONSTANTS.LOCATION_LIST_BODY;
        }
        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder ( ViewGroup viewGroup, int viewType ) {
        LayoutInflater mInflater = LayoutInflater.from (viewGroup.getContext ());

        if(viewType == LOCATIONRECORDERCONSTANTS.LOCATION_LIST_HEADER){
            ViewGroup vHeader = ( ViewGroup ) mInflater.inflate (R.layout.locations_list_header, viewGroup, false);
            HeaderLocationHolder vhHeader = new HeaderLocationHolder (vHeader);
            return vhHeader;
        } else {
            ViewGroup vBody = (ViewGroup) mInflater.inflate (R.layout.custom_location_item_layout, viewGroup, false);
            LocationHolder vhBody = new LocationHolder (vBody);
            return vhBody;
        }
    }

    //TODO - Update it
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LocationList current = mData.get(position);

        if(current.getTyp() == LOCATIONRECORDERCONSTANTS.LOCATION_LIST_BODY) {
            LocationHolder itemHolder = (LocationHolder) holder;
            itemHolder.mName.setText(current.getLocation().getName());
            itemHolder.mAddress.setText(current.getLocation().getAddress());

            itemHolder.mOnClickListener = this.mOnClickListener;
        }

        else{
            HeaderLocationHolder itemHolder = (HeaderLocationHolder) holder;
            itemHolder.mLetter.setText(current.getLetter() + "");
        }
    }
}



//ViewHolders

class  LocationHolder extends MainViewHolder implements View.OnClickListener {
    TextView mName;
    TextView mAddress;
    View mLine;

    public OnClickListener mOnClickListener;


    public LocationHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mName = (TextView) itemView.findViewById(R.id.locationName);
        mAddress = (TextView) itemView.findViewById(R.id.addressTextView);
    }

    @Override
    public void onClick(View view) {
        if (mOnClickListener != null) {
            mOnClickListener.onItemClicked(view, getPosition());
        }

    }
}



class  HeaderLocationHolder extends MainViewHolder {

    TextView mLetter;

    public HeaderLocationHolder(View itemView) {
        super(itemView);
        mLetter = (TextView) itemView.findViewById(R.id.headerLetter);
    }
}


