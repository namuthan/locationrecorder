package edu.yemen.nabilmuthanna1234.locationrecorder.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.activity.LocationDetailActivity;
import edu.yemen.nabilmuthanna1234.locationrecorder.adapter.LocationsAdapter;
import edu.yemen.nabilmuthanna1234.locationrecorder.adapter.LocationsListAdapter;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.OnClickListener;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Location;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.LocationList;


public class LocationsFragment extends Fragment implements OnClickListener {


    private RecyclerView recyclerView;
    private LocationsAdapter locationsAdapter;
    private List<Location> mLocations;

    private List<LocationList> mLocationsList;
    private LocationsListAdapter mLocationsListAdapter;
    // Database Helper
    DatabaseHelper db;


    RelativeLayout relativeLayout;
    public LocationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_locations, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.locationsList);
        setupDatabaseAndList();
        return view;
    }

    @Override
    public void onItemClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), LocationDetailActivity.class);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID, (int) mLocationsList.get(position).getLocation().getId());
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupDatabaseAndList();
    }

    public void setupDatabaseAndList (){

        db = new DatabaseHelper(this.getActivity());
        mLocationsList = new ArrayList<LocationList>();
        List<Location> locations = db.getAllLocations();
        Collections.sort(locations);

        char emptyLetter = '.';
        char[] letters;
        char letter = emptyLetter;
        if(!locations.isEmpty()) {
            if (locations.get(0) != null) {
                letters = locations.get(0).getName().toCharArray();
                letter = letters[0];
            }
        }
        mLocationsList.add(new LocationList(null, letter, LOCATIONRECORDERCONSTANTS.LOCATION_LIST_HEADER));

        for(int i = 0; i<locations.size(); i++){
           letters = locations.get(i).getName().toCharArray();
            if(letters[0] != letter){
                letter = letters[0];
                mLocationsList.add(new LocationList(null, letter, LOCATIONRECORDERCONSTANTS.LOCATION_LIST_HEADER));
            }

            mLocationsList.add(new LocationList(locations.get(i), emptyLetter, LOCATIONRECORDERCONSTANTS.LOCATION_LIST_BODY));
        }

        mLocationsListAdapter = new LocationsListAdapter(getActivity(), mLocationsList);
        mLocationsListAdapter.setOnClickListener(this);
        recyclerView.setAdapter(mLocationsListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }



}
