package edu.yemen.nabilmuthanna1234.locationrecorder.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.adapter.PlaceAutocompleteAdapter;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.LocationType;

// TODO Improve Google Suggestions to start with current city and outwards.
// TODO(Developer): Check error code and notify the user for google connections
// TODO : Check if Network is available
// TODO See what to run on Background threads
// TODO Extent getting current location to google play service and networking,  etc.




/***
 * Implements:
 *      GoogleApiClient, ConnectionsCallback,GoogleApiClient.OnConnectionFailedListener
 *          - > get places AutoComplete suggestions
 *      OnItemSelectedListener -> get selected items of a spinner list
 *      LocationListener -> get current address(last known address).
 */

public class RegisterNewLocationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, AdapterViewCompat.OnItemSelectedListener, LocationListener {

    private static final String TAG = "RegisterNewLocationActivity";

    //Inputs to database
    @InjectView(R.id.locationName) EditText mLocationName;
    @InjectView(R.id.addressTextField) AutoCompleteTextView mSourceLocation;
    @InjectView(R.id.locationTypesSpinner) Spinner mlocationTypesSpinner;
    private int mLocatioTypeId;
    private double mlatitude = 0;
    private double mLongtitude = 0;

    //Other UI views
    @InjectView(R.id.place_attribution) TextView mPlaceDetailsAttribution;
    @InjectView(R.id.imageView) ImageView mPoweredByGoogleImage;
    @InjectView(R.id.viewOnMap) ImageView mViewInMap;

    //Places Predictions - Google
    private GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;
    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    //Pick Place From map
    private final int PLACE_PICKER_REQUEST = 1;

    //Current Address
    private LocationManager mLocationManager;

    //Other Helper Variables
    private ArrayAdapter<String> adapter;
    private ArrayList<LocationType> mAllLocationTypes;

    // Database Helper
    private DatabaseHelper db;

    //Selected Address By user
    private String mSelectedLocationType = "default";
    private int gettingAddressMethod = -1;
    private  LatLng mAddressLatLan;
    private String mLocationTypeRangeUnite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new_location);
        ButterKnife.inject(this);

        mlatitude = 0;
        mLongtitude = 0;
        db = new DatabaseHelper(getApplicationContext());

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        saveLocationsTypesToDataBaseIfApplicable();

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mSourceLocation.setOnItemClickListener(mAutocompleteClickListener);
        mAdapter = new PlaceAutocompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, BOUNDS_GREATER_SYDNEY, null);
        mSourceLocation.setAdapter(mAdapter);

        showAddressLabel(false);
    }


    @OnClick(R.id.addLocationButton) void submit()
    {

        int duration = Toast.LENGTH_SHORT;
        Context context = getApplicationContext();

        if(mLocationName.getText().toString().equals("") || mSourceLocation.getText().toString().equals("")){
            Toast toast = Toast.makeText(context, "Make sure the name and address fields are filled"  , duration);
            toast.show();
            return;
        }

        if(gettingAddressMethod == LOCATIONRECORDERCONSTANTS.TYPE_ADDRESS) {
            mAddressLatLan = getLocationFromAddress(mSourceLocation.getText().toString());
            if (mAddressLatLan == null) {
                Toast toast = Toast.makeText(context, "Make sure the address is valid", duration);
                toast.show();
                return;
            }
        }

        //Save location info to database
        edu.yemen.nabilmuthanna1234.locationrecorder.model.Location location =
                new edu.yemen.nabilmuthanna1234.locationrecorder.model.Location();
        location.setName(mLocationName.getText().toString());
        location.setAddress(mSourceLocation.getText().toString());
        location.setLatitude(mAddressLatLan.latitude);
        location.setLongtitude(mAddressLatLan.longitude);
        location.setLocationTypeId(mAllLocationTypes.get(mLocatioTypeId).getID());
        location.setLevelOfImportance(LOCATIONRECORDERCONSTANTS.NORMAL);

        if(!locationExists(location)) {
            long locationId = db.createLocation(location);
            Toast toast = Toast.makeText(context, "Location Saved ", duration);
            toast.show();

            //Show the saved Location Details
            //Intent intent = new Intent(this, LocationDetailActivity.class);
            //intent.putExtra(LOCATIONRECORDERCONSTANTS.INTENT_LOCATION_ID, (int) locationId);
            //startActivity(intent);
            //finish();
        }
        else{
            Toast toast = Toast.makeText(context, "Location with this name already exists", duration);
            toast.show();
        }

        db.closeDB();
    }

    public boolean locationExists(edu.yemen.nabilmuthanna1234.locationrecorder.model.Location location)
    {
        List<edu.yemen.nabilmuthanna1234.locationrecorder.model.Location> locations = db.getAllLocations();

        for(int i = 0; i<locations.size(); i++){
            if(locations.get(i).getName().toLowerCase().compareTo(location.getName().toLowerCase()) == 0)
            {
                return true;
            }
        }
        return false;
    }


    @OnClick (R.id.addAddressBtn) void addAddress ()
    {

        //Show a dialog of the options available
        final String[] options = getResources().getStringArray(R.array.task_field_selections);
        final int selectedItemIndex[] = new int[1];
        selectedItemIndex[0] = -1;

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                RegisterNewLocationActivity.this);

        builderSingle.setTitle("Select One Option.");
        builderSingle.setSingleChoiceItems(R.array.add_address_options, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedItemIndex[0] = i;
            }
        });

        builderSingle.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                activateAppropriateAddressSelection(selectedItemIndex[0]);
            }
        });

        builderSingle.show();
    }

    private void activateAppropriateAddressSelection(int itemSelected) {

        //clear the address Input Text
        mSourceLocation.setText("");

        if(itemSelected == LOCATIONRECORDERCONSTANTS.TYPE_ADDRESS){
            gettingAddressMethod = LOCATIONRECORDERCONSTANTS.TYPE_ADDRESS;
            disableLocationManager();
            showAddressLabel(true);
        }

        if(itemSelected == LOCATIONRECORDERCONSTANTS.GET_CURRENT_ADDRESS){
            gettingAddressMethod = LOCATIONRECORDERCONSTANTS.GET_CURRENT_ADDRESS;
            Location location =  setUpLocationManagere();
            if(location != null){
                mlatitude = location.getLatitude();
                mLongtitude = location.getLongitude();
                setAddressFromLocation(location.getLatitude(), location.getLongitude(),
                        getApplicationContext());
            } else{
                Toast.makeText(this, "Unable to get the current address, you either need to enable your GPS or have a network access", Toast.LENGTH_LONG);
            }

            showAddressLabel(true);
        }

        if(itemSelected == LOCATIONRECORDERCONSTANTS.GET_ADDRESS_FROM_MAP) {
            gettingAddressMethod = LOCATIONRECORDERCONSTANTS.GET_ADDRESS_FROM_MAP;
            disableLocationManager();

            //Fire up Google Place Picket widget
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

            Context context = getApplicationContext();
            try {
                startActivityForResult(builder.build(context), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
                Toast toast = Toast.makeText(this, "Couldn't lanuch the map", Toast.LENGTH_LONG);
                toast.show();

            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
                Toast toast = Toast.makeText(this, "Couldn't lanuch the map" + itemSelected, Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

    @OnClick (R.id.cancelAddLocationBtn) void Cancel (){
        db.closeDB();
        finish();
    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                place.getPlaceTypes();
                String toastMsg = String.format("Place: %s", place.getName() + "with latlong " + place.getLatLng().toString());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                showAddressLabel(true);
                mSourceLocation.setText(place.getAddress());
                mAddressLatLan = place.getLatLng();
            }
        }
    }

    private void showAddressLabel(boolean b) {

        if(b){
            mSourceLocation.setVisibility(View.VISIBLE);
            mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
            mPoweredByGoogleImage.setVisibility(View.VISIBLE);
            mViewInMap.setVisibility(View.VISIBLE);
        } else{
            mPoweredByGoogleImage.setVisibility(View.INVISIBLE);
            mSourceLocation.setVisibility(View.INVISIBLE);
            mPlaceDetailsAttribution.setVisibility(View.INVISIBLE);
            mViewInMap.setVisibility(View.INVISIBLE);
        }
    }




    private Location setUpLocationManagere(){
        mLocationManager  = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean enabled = mLocationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check if enabled and if not send user to the GSP settings
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
        Location location=  mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location == null) {

            Toast.makeText(this, "Unable to get the current address ", Toast.LENGTH_LONG);
        } else {

            mAddressLatLan = new LatLng(location.getLatitude(), location.getLongitude());
        }
        return location;
    }

    public String setAddressFromLocation(final double latitude, final double longitude,
                                              final Context context) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String mCurrentAddressResult = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");
                }
                sb.append(address.getLocality()).append("\n");
                //sb.append(address.getPostalCode()).append("\n");
                sb.append(address.getCountryName());
                mCurrentAddressResult = sb.toString();
            }
        } catch (IOException e) {
            //Log.e(TAG, "Unable connect to Geocoder", e);
        } finally {
            if (mCurrentAddressResult != null) {
                mCurrentAddressResult = mCurrentAddressResult;
            } else {
                mCurrentAddressResult = "Unable to get address for this lat-long.";
            }

            mSourceLocation.setVisibility(View.VISIBLE);
            mSourceLocation.setText(mCurrentAddressResult);
            return mCurrentAddressResult;
        }
    }


    /** Location Delegates */
    @Override
    public void onItemSelected(AdapterViewCompat<?> parent, View view, int position, long id) {
        mSelectedLocationType = parent.getItemAtPosition(position).toString();
        mLocatioTypeId = position;
    }

    @Override
    public void onNothingSelected(AdapterViewCompat<?> parent) {
        mSelectedLocationType = "default";
        if(mAllLocationTypes != null){}
            //mLocatioTypeId = mAllLocationTypes.get(0).getID();
    }

    public  void disableLocationManager (){
        if(mLocationManager != null){
            mLocationManager.removeUpdates(this);
        }
    }
    /* Location Manager Delegate */
    @Override
    public void onLocationChanged(android.location.Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.viewOnMap) void viewOnMap (){

        //Show the choosen Location in map
        String address = mSourceLocation.getText().toString();
        mAddressLatLan = getLocationFromAddress(address);
        if(mAddressLatLan == null && gettingAddressMethod != LOCATIONRECORDERCONSTANTS.GET_CURRENT_ADDRESS){
            Toast.makeText(this, "Can't View in map beacuse address is not valid",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if(gettingAddressMethod == LOCATIONRECORDERCONSTANTS.GET_CURRENT_ADDRESS){
            mAddressLatLan = new LatLng(mlatitude, mLongtitude);
        }

        Bundle bundle = new Bundle();
        bundle.putDouble(LOCATIONRECORDERCONSTANTS.LOCATION_LATITUDE, mAddressLatLan.latitude);
        bundle.putDouble(LOCATIONRECORDERCONSTANTS.LOCATION_LONGTITUDE, mAddressLatLan.longitude);

        Intent intent = new Intent(this, FindMeInMap.class);
        intent.putExtras(bundle);

        startActivity(intent);

    }

    /**
     * Add New Location Type Handlers
     */
    @OnClick(R.id.addLocationType) void addLocationType (){

        //Get layout
        LayoutInflater factory = LayoutInflater.from(this);
        AlertDialog dialog = null;
        final View textEntryView = factory.inflate(R.layout.get_location_type_dialog_layout, null);

        final EditText input1 = (EditText) textEntryView.findViewById(R.id.locationTypeName);
        final EditText input2 = (EditText) textEntryView.findViewById(R.id.approximateDistance);
        final Spinner spinner = (Spinner) textEntryView.findViewById(R.id.uniteTypesSpinner);

        ArrayAdapter<String> unitTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.unite_types_options));
        unitTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(unitTypeAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mLocationTypeRangeUnite = getResources().getStringArray(R.array.unite_types_options)[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mLocationTypeRangeUnite = LOCATIONRECORDERCONSTANTS.LOCATION_TYPE_UNIT_METER;
            }
        });


        input1.setText("", TextView.BufferType.EDITABLE);
        input2.setText("", TextView.BufferType.EDITABLE);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setView(textEntryView)
                .setTitle("Add New Location Type")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        d.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button b = d.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if(!input1.getText().toString().equals("")){
                            //Update locationTypes List
                            int range = 0;
                            if(!input2.getText().toString().equals("")){
                                range =  Integer.parseInt(input2.getText().toString());
                            }
                            LocationType locationType = new LocationType(input1.getText().toString(),range
                                   , mLocationTypeRangeUnite);
                            if(!locationTypeExists(locationType)) {
                                db.createLocationType(locationType);

                                //Set as the index selected in the spinner list
                                setSpinnerAdapter();
                                mLocatioTypeId = adapter.getPosition(input1.getText().toString());
                                mlocationTypesSpinner.setSelection(mLocatioTypeId);
                                d.dismiss();
                            } else{
                                Toast.makeText(getApplicationContext(), "Location Type aleady exists, you can't fool me :P",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Location Name is required super user :)",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        d.show();
    }

    private boolean locationTypeExists(LocationType locationType){
        boolean found = false;
        ArrayList<String> names = getNames(db.getAllLocationTypes());
        for(int i = 0; i<names.size(); i++){
            String name1 = names.get(i).replaceAll("\\s+", "");
            String name2 = locationType.getLocationTypeName().replaceAll("\\s+", "");

            if(name1.compareToIgnoreCase
                    (name2) == 0){
                found =  true;
                break;
            }
        }
        return found;
    }

    private void saveLocationsTypesToDataBaseIfApplicable (){

        new Thread(new Runnable() {
            public void run() {
                List<LocationType> locationTypes = new ArrayList<LocationType>();
                final int size = locationTypes.size();
                locationTypes = db.getAllLocationTypes();
                if (locationTypes.size() <= 0) {
                    //add it to locationTypes table
                    String[] locationTypesArray = getResources().getStringArray(R.array.location_types);
                    for (int i = 0; i < locationTypesArray.length; i++) {
                        db.createLocationType(new LocationType(locationTypesArray[i], 100, "m"));
                    }
                }

                runOnUiThread(new Runnable() {
                    public void run() {
                        //Toast.makeText(getApplicationContext(), "Location Types Number is" + size, Toast.LENGTH_LONG).show();
                        //Populate location types spinner
                        setSpinnerAdapter();
                    }
                });
            }
        }).start();
    }

    private ArrayList<String>  setSpinnerAdapter() {
        mAllLocationTypes = db.getAllLocationTypes();
        Collections.sort(mAllLocationTypes);

        ArrayList<String> names = getNames(mAllLocationTypes);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mlocationTypesSpinner.setAdapter(adapter);

        mlocationTypesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mLocatioTypeId = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mLocatioTypeId = 0;
            }
        });

        return names;
    }

    private ArrayList<String> getNames(List<LocationType> allLocationTypes) {

        ArrayList<String> names = new ArrayList<String>();
        for (int i = 0; i < allLocationTypes.size(); i++) {
            names.add(allLocationTypes.get(i).getLocationTypeName());
        }
        return names;
    }



    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceAutocompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Toast.makeText(getApplicationContext(), "Clicked: " + item.description,
                    Toast.LENGTH_SHORT).show();
        }
    };


    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                //Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // Format details of the place for display and show it in a TextView.
            //mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
            //place.getId(), place.getAddress(), place.getPhoneNumber(),
            //place.getWebsiteUri()));

            // Display the third party attributions if set.
            final CharSequence thirdPartyAttribution = places.getAttributions();
            if (thirdPartyAttribution == null) {
                mPlaceDetailsAttribution.setVisibility(View.GONE);
            } else {
                mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
                mPlaceDetailsAttribution.setText(Html.fromHtml(thirdPartyAttribution.toString()));
            }

            places.release();
        }
    };

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }



    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public LatLng getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }


}



