package edu.yemen.nabilmuthanna1234.locationrecorder.model;

/**
 * Created by nabil on 2015-06-22.
 */
public class LocationList {



    private Location mLocation;
    private char mLetter;
    private int mTyp;

    public LocationList (Location location, char letter, int type){
        this.mLocation = new Location(location);
        this.mLetter = letter;
        this.mTyp = type;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location mLocation) {
        this.mLocation = mLocation;
    }

    public char getLetter() {
        return mLetter;
    }

    public void setLetter(char mLetter) {
        this.mLetter = mLetter;
    }

    public int getTyp() {
        return mTyp;
    }

    public void setTyp(int typ) {
        this.mTyp = typ;
    }
}
