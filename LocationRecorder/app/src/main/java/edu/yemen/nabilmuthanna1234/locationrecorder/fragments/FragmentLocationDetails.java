package edu.yemen.nabilmuthanna1234.locationrecorder.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLocationDetails extends Fragment {


    public FragmentLocationDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location_details, container, false);
    }


}
