package edu.yemen.nabilmuthanna1234.locationrecorder.model;

/**
 * Created by nabil on 2015-06-27.
 */
public class LocationType implements Comparable{

    private String mLocationTypeName;
    private int mRange;
    private String mUnitType;
    private int mID;

    public LocationType(){

    }

    public LocationType (String locationTypeName, int range, String unitType){
        this.mLocationTypeName = locationTypeName;
        this.mRange = range;
        this.mUnitType = unitType;
    }

    public String getLocationTypeName() {
        return mLocationTypeName;
    }

    public void setLocationTypeName(String mLocationTypeName) {
        this.mLocationTypeName = mLocationTypeName;
    }

    public int getRange() {
        return mRange;
    }

    public void setRange(int mRange) {
        this.mRange = mRange;
    }

    /**
     * Helpers Functions
     */
    public int compareTo(Object anotherLocation) throws ClassCastException {
        if (!(anotherLocation instanceof LocationType))
            throw new ClassCastException("A Location object expected.");
        LocationType locationType  = (LocationType) anotherLocation;

        return this.getLocationTypeName().compareToIgnoreCase(locationType.getLocationTypeName());
    }

    public String getUnitType() {
        return mUnitType;
    }

    public void setUnitType(String mUnitType) {
        this.mUnitType = mUnitType;
    }

    public int getID() {
        return mID;
    }

    public void setID(int ID) {
        this.mID = ID;
    }
}
