package edu.yemen.nabilmuthanna1234.locationrecorder.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Task;

public class TaskDetailActivity extends Activity {

    @InjectView(R.id.taskTitle) TextView mTaskTitle;
    @InjectView(R.id.taskLocation) TextView mTaskLocation;
    @InjectView(R.id.taskSummary) TextView mTaskSummary;
    @InjectView(R.id.taskDescription) TextView mTaskDescription;


    private DatabaseHelper db;
    private int mTaskId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);
        ButterKnife.inject(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if (!extras.isEmpty()) {
                mTaskId = extras.getInt(LOCATIONRECORDERCONSTANTS.INTENT_TASK_ID);
            }
        }
    }

    @OnClick(R.id.backArrowImageView) void finshActivity (){
        finish();
    }

    @OnClick(R.id.editImageView) void editLocation (){
        //Start EditLocation Activity
        Intent intent = new Intent(TaskDetailActivity.this, EditTaskActivity.class);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.EDIT_TASK_ID, (int)mTaskId);
        intent.putExtra(LOCATIONRECORDERCONSTANTS.EDIT_TASK_TITLE, mTaskTitle.getText().toString());
        intent.putExtra(LOCATIONRECORDERCONSTANTS.EDIT_TASK_SUMMARY, mTaskSummary.getText().toString());
        intent.putExtra(LOCATIONRECORDERCONSTANTS.EDIT_TASK_DESCRIPTION, mTaskDescription.getText().toString());
        startActivity(intent);
    }

    @OnClick(R.id.deleteLocation) void deletLocation (){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure, you want to delete location ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Delete this location
                        //db.deleteTask(mTaskId);
                        finish();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create();
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Get the data from database
        db = new DatabaseHelper(this);

        Task task = db.getTask(mTaskId);
        mTaskTitle.setText(task.getTitle());
        //mTaskLocation.setText(task.getLocationName());
        mTaskSummary.setText(task.getSummary());
        mTaskDescription.setText(task.getDescription());
    }
}
