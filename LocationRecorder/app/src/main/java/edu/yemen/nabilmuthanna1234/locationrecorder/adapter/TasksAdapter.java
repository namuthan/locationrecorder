package edu.yemen.nabilmuthanna1234.locationrecorder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.OnClickListener;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Task;

/**
 * Created by nabil on 2015-06-20.
 */
public class TasksAdapter extends RecyclerView.Adapter<TaskHolder> {

    List<Task> mData= Collections.emptyList();
    private LayoutInflater mInflater;
    private Context mContext;

    boolean mAllTaks;
    public OnClickListener mOnClickListener;

    public void setOnClickListener (OnClickListener onClickListener){
        this.mOnClickListener = onClickListener;
    }

    public TasksAdapter(Context context, List<Task> data){

        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        mAllTaks = true;
    }

    public void setListType (boolean allTasks){
        this.mAllTaks = allTasks;
    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.custom_task_item_layout, parent,false);
        TaskHolder holder=new TaskHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {
        TaskHolder itemHolder= (TaskHolder) holder;
        Task current = mData.get(position);

        itemHolder.mTitle.setText(current.getTitle());
        if(mAllTaks){
             //itemHolder.mLocationName.setText(current.getLocationName());
        } else {
            itemHolder.mLocationName.setText(current.getSummary());
        }
        holder.mOnClickListener = this.mOnClickListener;
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

}


class  TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView mTitle;
    TextView mLocationName;
    TextView mSummary;
    public OnClickListener mOnClickListener;


    public TaskHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mTitle = (TextView) itemView.findViewById(R.id.taskTitle);
        mLocationName = (TextView) itemView.findViewById(R.id.locationNameForTask);
    }

    @Override
    public void onClick(View view) {
        if (mOnClickListener != null) {
            mOnClickListener.onItemClicked(view, getPosition());
        }

    }
}



class  HeaderTaskHolder extends RecyclerView.ViewHolder {

    TextView mLocationName;
    public OnClickListener mOnClickListener;

    public HeaderTaskHolder(View itemView) {
        super(itemView);
        mLocationName = (TextView) itemView.findViewById(R.id.headerLocationName);
    }

}
