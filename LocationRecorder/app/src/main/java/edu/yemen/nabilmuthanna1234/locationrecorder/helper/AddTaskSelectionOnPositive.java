package edu.yemen.nabilmuthanna1234.locationrecorder.helper;

import java.util.ArrayList;

/**
 * Created by nabil on 2015-06-25.
 */
public interface AddTaskSelectionOnPositive {
    public void onSelectionsFinished (ArrayList<String> selection);
}
