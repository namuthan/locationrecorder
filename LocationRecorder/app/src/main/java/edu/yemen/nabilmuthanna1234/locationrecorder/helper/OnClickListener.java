package edu.yemen.nabilmuthanna1234.locationrecorder.helper;

import android.view.View;

/**
 * Created by nabil on 2015-06-20.
 */
public interface OnClickListener {

    public void onItemClicked (View view, int position);
}
