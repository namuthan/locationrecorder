package edu.yemen.nabilmuthanna1234.locationrecorder.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.InjectView;
import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;

public class FindMeInMap extends AppCompatActivity  implements LocationListener {

    private static String TAG = MainActivity.class.getSimpleName();


    @InjectView(R.id.toolbar) Toolbar mToolbar;
    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private String mProvider;
    private Marker mNow;
    private boolean findMeinMap = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_me_in_map);
        ButterKnife.inject(this);

        setSupportActionBar(mToolbar);

        Bundle extras = getIntent().getExtras();

        if(extras != null) {
            if (!extras.isEmpty()) {
                findMeinMap = false;
                setUpLocationManagerWithGoogleMap();

                double latitude = extras.getDouble(LOCATIONRECORDERCONSTANTS.LOCATION_LATITUDE);
                double lontitude = extras.getDouble(LOCATIONRECORDERCONSTANTS.LOCATION_LONGTITUDE);

                LatLng CURRENT_LOCATION = new LatLng(latitude, lontitude);

                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                mNow = mMap.addMarker(new MarkerOptions().position(CURRENT_LOCATION).title("Find me here!"));
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(CURRENT_LOCATION, 14);
                mMap.animateCamera(update);
            }
        } else {
            setUpLocationManagerWithGoogleMap();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        //mLocationManager.requestLocationUpdates(mProvider, 90000, 1, this);
    }

    protected void onRestart (){
        super.onRestart();
        //mLocationManager.requestLocationUpdates(mProvider, 90000, 1, this);
    }

    protected  void onStart () {
        super.onStart();
        //mLocationManager.requestLocationUpdates(mProvider, 90000, 1, this);

    }
    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        if(findMeinMap) mLocationManager.removeUpdates(this);
        //mMap;
    }

    @Override
    public void onLocationChanged(Location location) {
        //Remove marker in the map if found
        if(findMeinMap) {
            if (mNow != null) {
                mNow.remove();

            }

            LatLng CURRENT_LOCATION = new LatLng(location.getLatitude(), location.getLongitude());

            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            mNow = mMap.addMarker(new MarkerOptions().position(CURRENT_LOCATION).title("Find me here!"));
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(CURRENT_LOCATION, 14);
            mMap.animateCamera(update);

            Toast.makeText(this, "New Location Updates " + location.getLatitude() + " "
                            + location.getLongitude(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }


    private void setUpLocationManagerWithGoogleMap(){
        mMap  = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        if(findMeinMap){
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            mLocationManager  = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean enabled = mLocationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // check if enabled and if not send user to the GSP settings
            if (!enabled) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }

            Criteria criteria = new Criteria();
            mProvider = mLocationManager.getBestProvider(criteria, false);
            mLocationManager.requestLocationUpdates(mProvider, 1000000, 10,
                this);
        }
    }
}
