package edu.yemen.nabilmuthanna1234.locationrecorder.helper;

/**
 * Created by nabil on 2015-06-20.
 */
public class LOCATIONRECORDERCONSTANTS {

    //INTENTS
    public static final String INTENT_LOCATION_NAME = "name";
    public static final String INTENT_LOCATION_ADDRESS = "address";
    public static final String INTENT_LOCATION_ID = "location_id";
    public static final String INTENT_TASK_ID = "task_id";
    public static final String START_ACTIVTY = "location_id";

    //EDITS
    public static final String MY_PREFS_NAME = "RefName";
    public static final String MY_FRAGMENT_ID = "fragment_id";
    public static final String EDIT_LOCATION_LOCATION_NAME = "edit_location_name";
    public static final String EDIT_LOCATION_ADDRESS = "edit_location_address";

    public static final String EDIT_TASK_ID = "edit_task_id";
    public static final String EDIT_TASK_TITLE = "edit_task_title";
    public static final String EDIT_TASK_SUMMARY = "edit_task_summary";
    public static final String EDIT_TASK_DESCRIPTION = "edit_task_description";

    //FRAGMENTS ID
    public static final int LOCATION_FRAGMENT_ID = 1;
    public static final int TASK_FRAGMENT_ID = 2;
    public static final int HOME_FRAGMENT_ID = 0;


    //List Type
    public static final int TASK_LIST_HEADER = 1;
    public static final int TASK_LIST_BODY = 2;

    public static final int LOCATION_LIST_HEADER = 1;
    public static final int LOCATION_LIST_BODY = 2;


    public static final String LOCATION_TYPE_UNIT_METER = "m";
    public static final String LOCATION_TYPE_UNIT_KM = "km";

    //Getting address options
    public static final int TYPE_ADDRESS = 0;
    public static final int GET_CURRENT_ADDRESS = 1;
    public static final int GET_ADDRESS_FROM_MAP = 2;

    public static final String LOCATION_LATITUDE = "location_latitude";
    public static final String LOCATION_LONGTITUDE = "location_longtitude";

    public static final int NORMAL = 0;
    public static final int IMPORTANT = 1;


    //Task Class Variables
    public static final int TASK_NO_PRIORITY_ADDED = -1;
    public static final String TASK_NO_DEADLINE_ADDED = "no_deadline_added";

    public static final int TASK_PROGRESS_ADDED = 100;
    public static final int TASK_PROGRESS_PROGRESS = 200;
    public static final int TASK_PROGRESS_DONE = 300;
}
