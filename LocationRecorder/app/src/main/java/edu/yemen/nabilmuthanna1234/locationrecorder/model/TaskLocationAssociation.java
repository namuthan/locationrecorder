package edu.yemen.nabilmuthanna1234.locationrecorder.model;

/**
 * Created by nabil on 2015-06-30.
 */
public class TaskLocationAssociation {

    private int mId;
    private int mLocationId;
    private int mTaskId;
    private String mCreatedAt;

    //Empty constructor
    public TaskLocationAssociation (){}

    public TaskLocationAssociation (int locationId, int taskId){
        this.mLocationId = locationId;
        this.mTaskId = taskId;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public int getLocationId() {
        return mLocationId;
    }

    public void setLocationId(int mLocationId) {
        this.mLocationId = mLocationId;
    }

    public int getTaskId() {
        return mTaskId;
    }

    public void setTaskId(int mTaskId) {
        this.mTaskId = mTaskId;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }


}
