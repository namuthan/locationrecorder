package edu.yemen.nabilmuthanna1234.locationrecorder.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

import edu.yemen.nabilmuthanna1234.locationrecorder.activity.AddNewTaskActivity;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.AddTaskSelectionOnPositive;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.model.Location;

/**
 * Created by nabil on 2015-06-28.
 */
public class LocationsMultiSelectionList  extends DialogFragment {

    ArrayList<Location> mSelections = new ArrayList<Location>();
    AddTaskSelectionOnPositive selectionsListener;

    @Override
    public Dialog onCreateDialog (Bundle savedInstance){

        DatabaseHelper db = new DatabaseHelper(getActivity());
        final List<Location> locations = db.getAllLocations();

        final String[] items = getLocationsNames(locations);

        AlertDialog.Builder  builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Location").setMultiChoiceItems(items, null,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override

                    public void onClick(DialogInterface dialogInterface, int i, boolean isChecked) {
                        if(isChecked){
                            //Add it to mSelections
                            mSelections.add(locations.get(i));
                        } else{
                            if(mSelections.contains(locations.get(i))){
                                mSelections.remove(locations.get(i));
                            }
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AddNewTaskActivity parent = (AddNewTaskActivity)getActivity();
                parent.setLocationSelections(mSelections);
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AddNewTaskActivity parent = (AddNewTaskActivity)getActivity();
                mSelections.clear();
                //parent.setLocationSelections(mSelections);
            }
        });
        return builder.create();
    }

    private String[] getLocationsNames(List<Location> locations) {
        String names[] = new String[locations.size()];

        for(int i = 0; i< locations.size(); i++){
            names[i] = locations.get(i).getName();
        }
        return names;
    }
}
