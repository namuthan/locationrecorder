package edu.yemen.nabilmuthanna1234.locationrecorder.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.DatabaseHelper;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.LOCATIONRECORDERCONSTANTS;

public class EditTaskActivity extends AppCompatActivity {

    @InjectView(R.id.taskTitle) EditText mTaskTitle;
    @InjectView(R.id.taskSummary) EditText mTaskSummary;
    @InjectView(R.id.taskDescription) EditText mTaskDescription;

    private int mTaskId;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);
        ButterKnife.inject(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if (!extras.isEmpty()) {
                mTaskId = extras.getInt(LOCATIONRECORDERCONSTANTS.EDIT_TASK_ID);
                mTaskTitle.setText(extras.getString(LOCATIONRECORDERCONSTANTS.EDIT_TASK_TITLE));
                mTaskSummary.setText(extras.getString(LOCATIONRECORDERCONSTANTS.EDIT_TASK_SUMMARY));
                mTaskDescription.setText(extras.getString(LOCATIONRECORDERCONSTANTS.EDIT_TASK_DESCRIPTION));
            }
        }
    }

    @OnClick(R.id.cancelTaskBtn) void finshActivity (){
        finish();
    }

    @OnClick(R.id.saveTaskBtn) void saveEdits (){
    //        db = new DatabaseHelper(this);
//        Task task = db.getTask(mTaskId);
//
//        task.setId(mTaskId);
//        task.setTitle(mTaskTitle.getText().toString());
//        task.setSummary(mTaskSummary.getText().toString());
//        task.setDescription(mTaskDescription.getText().toString());
//
//        db.updateTask(task);
        finish();
    }
}
