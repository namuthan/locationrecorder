package edu.yemen.nabilmuthanna1234.locationrecorder.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

import edu.yemen.nabilmuthanna1234.locationrecorder.R;
import edu.yemen.nabilmuthanna1234.locationrecorder.activity.AddNewTaskActivity;
import edu.yemen.nabilmuthanna1234.locationrecorder.helper.AddTaskSelectionOnPositive;

/**
 * Created by nabil on 2015-06-25.
 */
public class MultiSelectionTaskFields extends DialogFragment {

    ArrayList<String> mSelections = new ArrayList<String>();
    AddTaskSelectionOnPositive selectionsListener;

    @Override
    public Dialog onCreateDialog (Bundle savedInstance){
        final String[] items = getResources().getStringArray(R.array.task_field_selections);

        AlertDialog.Builder  builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Field").setMultiChoiceItems(R.array.task_field_selections, null,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override

                    public void onClick(DialogInterface dialogInterface, int i, boolean isChecked) {
                        if(isChecked){
                            //Add it to mSelections
                            mSelections.add(items[i]);
                        } else{
                            if(mSelections.contains(items[i])){
                                mSelections.remove(items[i]);
                            }
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AddNewTaskActivity parent = (AddNewTaskActivity)getActivity();
                parent.setmSelections(mSelections);
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AddNewTaskActivity parent = (AddNewTaskActivity)getActivity();
                mSelections.clear();
                parent.setmSelections(mSelections);
            }
        });
        return builder.create();
    }
}
