package edu.yemen.nabilmuthanna1234.locationrecorder.model;


/**
 * Created by nabil on 2015-06-18.
 */
public class Task implements Comparable{

    //Private Variables
    private int mId;
    private String mTitle;
    private String mDescription;
    private String mSummary;
    private float mPriority;
    private String mDeadLine;
    private int mLevelOfProgress;
    private int mLevelOfImportance;
    private String mCreatedAt;



    public Task() {
    }

    //Copy Constructor
    public Task(Task anotherTask){
        if(anotherTask != null) {
            this.mId = anotherTask.getId();
            this.mTitle = anotherTask.getTitle();
            this.mDescription = anotherTask.getDescription();
            this.mSummary = anotherTask.getSummary();
            this.mCreatedAt = anotherTask.getCreatedAt();
            this.mLevelOfImportance = anotherTask.getLevelOfImportance();
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getSummary() {
        return mSummary;
    }

    public void setSummary(String mSummary) {
        this.mSummary = mSummary;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public int getLevelOfImportance() {
        return mLevelOfImportance;
    }

    public void setLevelOfImportance(int mLevelOfImportance) {
        this.mLevelOfImportance = mLevelOfImportance;
    }

    public int compareTo(Object anotherTask) throws ClassCastException {
        if (!(anotherTask instanceof Task))
            throw new ClassCastException("A Task object expected.");
        Task task = (Task) anotherTask;

        return this.getTitle().compareToIgnoreCase(task.getTitle());
    }

    public float getPriority() {
        return mPriority;
    }

    public void setPriority(float mPriority) {
        this.mPriority = mPriority;
    }

    public String getDeadLine() {
        return mDeadLine;
    }

    public void setDeadLine(String mDeadLine) {
        this.mDeadLine = mDeadLine;
    }

    public int getLevelOfProgress() {
        return mLevelOfProgress;
    }

    public void setLevelOfProgress(int mLevelOfProgress) {
        this.mLevelOfProgress = mLevelOfProgress;
    }
}
